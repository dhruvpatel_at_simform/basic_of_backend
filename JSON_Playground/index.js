let todosList = [];

const form = document.querySelector("form");
const tableBody = document.querySelector("tbody");
const saveLocalButton = document.querySelector("#save-local");
// console.log("let me click", submitButton);

async function fetchAndSetTodos(from, to) {
  const todosResponse = await fetch(
    `https://jsonplaceholder.typicode.com/users/1/todos`
  );
  const todos = await todosResponse.json();
  // console.log(todos);
  todosList = todos;
}
const insertRawTemplate = (...args) => {
  const raw = document.createElement("tr");
  args.forEach((arg) => {
    const td = document.createElement("td");
    const textNode = document.createTextNode(arg);
    td.appendChild(textNode);
    raw.appendChild(td);
  });
  if (!args[2]) {
    raw.classList.add("red");
  } else {
    raw.classList.add("green");
  }
  tableBody.appendChild(raw);
};

const filterAndInsertRaws = (from, to) => {
  todosList = todosList.filter((todo) => todo.id >= from && todo.id <= to);
  todosList.forEach(({ id, title, completed }) =>
    insertRawTemplate(id, title, completed)
  );
};

function formHandler(event) {
  event.preventDefault();
  todosList = [];
  for (let i = tableBody.childElementCount; i > 1; i--) {
    tableBody.deleteRow(-1);
  }
  let from = parseInt(event.target[0].value || 1);
  let to = parseInt(event.target[1].value || 10);
  if (from > to) {
    console.log("here inside if");
    const temp = from;
    from = to;
    to = temp;
  }
  fetchAndSetTodos().then(() => filterAndInsertRaws(from, to));
}

const saveLocalHandler = () => {
  if (todosList.length === 0) {
    return;
  }
  const jsonData = JSON.stringify(todosList);
  localStorage.setItem("local-data", jsonData);
  alert("Your data saved successfully (:");
};

form.addEventListener("submit", formHandler);
saveLocalButton.addEventListener("click", saveLocalHandler);

window.onload = function () {
  const localTodos = localStorage.getItem("local-data");
  console.log("inside oload", localTodos);
  if (localTodos) {
    todosList = JSON.parse(localTodos);
    filterAndInsertRaws(todosList[0].id, todosList[todosList.length - 1].id);
  }
  return;
};
